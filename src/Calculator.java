
import java.util.Scanner;

/* 1. Калькулятор умеет выполнять операции сложения, вычитания, умножения и деления с двумя числами:
a + b, a - b, a * b, a / b. Данные передаются в одну строку (смотри пример)!
Решения, в которых каждое число и арифмитеческая операция передаются с новой строки считаются неверными.

2. Калькулятор умеет работать как с арабскими (1,2,3,4,5…), так и с римскими (I,II,III,IV,V…) числами.
3. Калькулятор должен принимать на вход числа от 1 до 10 включительно, не более.
На выходе числа не ограничиваются по величине и могут быть любыми.

4. Калькулятор умеет работать только с целыми числами.
5. Калькулятор умеет работать только с арабскими или римскими цифрами одновременно, при вводе пользователем строки вроде 3 + II калькулятор должен выбросить исключение и прекратить свою работу.
При вводе римских чисел, ответ должен быть выведен римскими цифрами, соответственно,
при вводе арабских - ответ ожидается арабскими.

6. При вводе пользователем неподходящих чисел приложение выбрасывает исключение и завершает свою работу.
7. При вводе пользователем строки, не соответствующей одной из вышеописанных арифметических операций,
 приложение выбрасывает исключение и завершает свою работу.

8. Результатом операции деления является целое число, остаток отбрасывается.
9. Результатом работы калькулятора с арабскими числами могут быть отрицательные числа и ноль.
10. Результатом работы калькулятора с римскими числами могут быть только положительные числа, если результат работы меньше единицы, выбрасывается исключение*/
public class Calculator {
    public static void main(String[] args) {
        System.out.println("Введите данные либо арабские либо римских цифр. По 2 аргумента и между ними один операнд через пробел");
        Scanner sc = new Scanner(System.in);

        String a1 = sc.next(); // получение данных с консоли
        char a2 = a1.charAt(0);// нужно для определения введены римские или арабские цифры. Берется первый символ и присваевается а2.
        char operator = sc.next().charAt(0); // получение данных с консоли. Полдучение оператора
        String b1 = sc.next(); // получение данных с консоли
        char b2 = a1.charAt(0); // нужно для определения введены римские или арабские цифры
        if (a2 == 'I' || a2 == 'X' || a2 == 'V'
                && b2 == 'V' || b2 == 'I' || b2 == 'X') { // если первый символ взятый из консоли совпадает с римским символом
            // то вызывается метод roman
            roman(a1, b1, operator);

        } else {
            long a = 0;
            long b = 0;
            try {// Обработка исключения при введении неверного символа или неверной строки tru catch
                a = Long.parseLong(a1);
                b = Long.parseLong(b1);
            } catch (NumberFormatException e) {
                System.out.println("Неправильно введены данные!");
                System.out.println("Введите данные либо арабские либо римских цифр. " +
                        "По 2 аргумента и между ними один операнд через пробел");
            }

            switch (operator) {// сам калькулятор с обработкой арабских цифр
                case '+':
                    System.out.println(a + b);
                    break;
                case '-':
                    System.out.println(a - b);
                    break;
                case '*':
                    System.out.println(a * b);
                    break;
                case '/':
                    if (a == 0 || b == 0) {
                        System.out.println("Division by 0!");
                        break;
                    } else {
                        System.out.println(a / b);
                        break;
                    }
                default:
                    System.out.println("Unknown operator");
            }
        }
    }

    private static void roman(String a1, String b1, char operator) {//Калькулятор по обработке римских цифр
        int index_a = 0, index_b = 0, rez;
        String rez1 = null;
        int i1;
        String[] rome = {"I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X"};

        for (int i = 0; i < rome.length; i++) {
            if (a1.equals(rome[i])) {
                index_a = i + 1;
            }
            if (b1.equals(rome[i])) {
                index_b = i + 1;
            }
        }
        switch (operator) {
            case '+':
                rez = index_a + index_b; //index - это целое арабское число, складываем получаем результат
                for (int i = 0; i <= rez; i++) {
                    if (rez == i && rez <= 10) {
                        rez1 = rome[i - 1];
                    } else if (rez > 10 && rez < 20) {
                        i1 = rez - 10;
                        rez1 = "X" + rome[i1 - 1];
                    }
                    else if (rez == 20){
                        rez1 = "XX";
                    }
                }
                System.out.println(rez1);
                break;
            case '-':
                rez = index_a - index_b;
                for (int i = 0; i <= rez; i++) {
                    if (rez == i && rez < 10) {
                        rez1 = rome[i - 1];
                    }
                }
                System.out.println(rez1);
                break;
            case '*': // Умножение как и говорилось в звдвче возможно с результатом до 100
                rez = index_a * index_b;
                for (int i = 0; i <= rez + 1; i++) {
                    if (rez == i && rez < 10) {
                        rez1 = rome[i - 1];
                    } else if (rez >= 10 && rez < 20) {
                        i1 = rez - 10;
                        rez1 = "X" + rome[i1 - 1];

                    } else if (rez == 20) {
                        rez1 = "XX";
                    } else if (rez > 20 && rez < 30) {
                        i1 = rez - 20;
                        rez1 = "XX" + rome[i1 - 1];

                    } else if (rez == 30) {
                        rez1 = "XXX";
                    } else if (rez > 30 && rez < 40) {
                        i1 = rez - 30;
                        rez1 = "XXX" + rome[i1 - 1];

                    } else if (rez == 40) {
                        rez1 = "XL";
                    } else if (rez > 40 && rez < 50) {
                        i1 = rez - 40;
                        rez1 = "XL" + rome[i1 - 1];

                    } else if (rez == 50) {
                        rez1 = "L";
                    } else if (rez > 50 && rez < 60) {
                        i1 = rez - 50;
                        rez1 = "L" + rome[i1 - 1];

                    } else if (rez == 60) {
                        rez1 = "LX";
                    } else if (rez > 60 && rez < 70) {
                        i1 = rez - 60;
                        rez1 = "LX" + rome[i1 - 1];

                    } else if (rez == 70) {
                        rez1 = "LXX";
                    } else if (rez > 70 && rez < 80) {
                        i1 = rez - 70;
                        rez1 = "LXX" + rome[i1 - 1];

                    } else if (rez == 80) {
                        rez1 = "LXXX";
                    } else if (rez > 80 && rez < 90) {
                        i1 = rez - 80;
                        rez1 = "LXXX" + rome[i1 - 1];

                    } else if (rez == 90) {
                        rez1 = "XC";
                    } else if (rez > 90 && rez < 100) {
                        i1 = rez - 90;
                        rez1 = "XC" + rome[i1 - 1];
                    }
                    if (rez == 100) {
                        rez1 = "C";
                    }
                }
                System.out.println(rez1);
                break;
            case '/':
                rez = index_a / index_b;
                if (rez <= 0) {
                    System.out.println("In the Roman system of calculus there are no zero and negative numbers");
                    break;
                }
                for (int i = 0; i <= rez; i++) {
                    if (rez == i) {
                        rez1 = rome[i - 1];
                    }
                }
                System.out.println(rez1);
                break;
            default:
                System.out.println("Unknown operator");
        }
    }
}

